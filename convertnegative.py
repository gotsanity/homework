from graphics import *

def main():
# prompt the user for the file to convert
	imageFilename = input("Please enter the filename to convert: ")
# read the file into the screen

	image = Image(Point(0,0), imageFilename)
	image.move(image.getWidth() / 2, image.getHeight() / 2)
	win = GraphWin("Convert to Negative", image.getWidth(), image.getHeight())
	win.setBackground("white")
	image.draw(win)
	wait = win.getMouse()

	for row in range(image.getWidth()):
		for column in range(image.getHeight()):
			r, g, b = image.getPixel(row, column)
			image.setPixel(row, column, color_rgb(255-r, 255-g, 255-b))
		win.update()

	filename = input("What name (including extension of .gif or .png) should we save the image as? ")
	image.save(filename)


main()